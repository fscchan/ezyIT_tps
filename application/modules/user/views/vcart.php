<?php
	$listcart = GetCart();
	$config = GetConfig();
	$selectcurrency = SelectedCurrency();
	$path = GetCurrencyPath(false, true);
	$this->load->model("m_product");
	$hasqtydisc = false;
	foreach ($listcart as $list) {
		if ($list["qtydisc"] > 0) {
			$hasqtydisc = true;
		}
	}
?>

<div id="content">  
    <div class="breadcrumb">
        <a href="<?php echo base_url() ?>">Home</a>
        » <a href="<?php echo base_url() . 'index.php/user/cart' ?>">Shopping Cart</a>
    </div>
    <h1>Shopping Cart
    </h1>
    <form  method="post" enctype="multipart/form-data">
        <div class="cart-info">
            <table>
                <thead>
                    <tr>
                        <td class="image">Image</td>
                        <td class="name">Name / Model</td>
                        <td class="quantity">Qty</td>
                        <td class="price">Price</td>
						<?php if ($hasqtydisc) { echo "<td class=\"bulkdisc\">Bulk discount</td>"; } ?>
                        <td class="total">Total</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (count($listcart) > 0) {
                        foreach ($listcart as $cartsatuan) {
							$product = $this->m_product->GetOneProduct($cartsatuan['id']);
                            ?>

                            <tr>
                                <td class="image">              
                                    <a href="<?php echo base_url() . 'index.php/user/view_product/' . $cartsatuan['id'] . '?' . $path . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $cartsatuan['name']) . '.html' ?>">
                                        <img style="height:50px;" src="<?php echo $config['folderproduct'] . (@$cartsatuan['product_image'] != '' ? $cartsatuan['product_image'] : 'default.jpg') ?>" alt="London Bus Money Bank" title="<?php echo $cartsatuan['name'] ?>"></a>
                                </td>
                                <td class="name"><a href="<?php echo base_url() . 'index.php/user/view_product/' . $cartsatuan['id'] . '?' . $path . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $cartsatuan['name']) . '.html' ?>"><?php echo $cartsatuan['name'] ?></a>
                                    <div>
                                    </div>
                                </td>
                                <td class="quantity">
                                    <input type="number" onkeypress="return isNumberKey(event)"  class="txt_qty" id="txt_qty<?php echo $cartsatuan['id'] ?>"  value="<?php echo $cartsatuan['qty'] ?>" size="2" min="<?php echo $product->product_min_stock; ?>" max="99">
                                    &nbsp;
                                    <input type="image" data-id="<?php echo $cartsatuan['id'] ?>" class="btt_update" src="<?php echo base_url() . 'images/update.png' ?>" alt="Update" title="Update">
                                    &nbsp;<img data-id="<?php echo $cartsatuan['id'] ?>" class="btt_remove" src="<?php echo base_url() . 'images/remove.png' ?>" alt="Remove" title="Remove"></td>
                                <td class="price">
                                    <?php if ($cartsatuan['normal_price'] > $cartsatuan['price']) { ?>
                                        <span class="price-old">
                                            <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['normal_price']), $selectcurrency) ?>
                                        </span>
                                    <?php } ?>
                                    <span class="price-new">

                                        <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['price']), $selectcurrency) ?>
                                    </span>

                                </td>
								
								<?php if ($hasqtydisc) { ?>
									<td class="bulkdisc"><?php echo $cartsatuan["qtydisc"] + 0; ?> % off</td>
								<?php } ?>

                                <td class="total">
                                    <?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $cartsatuan['subtotal']), $selectcurrency); ?>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td class="image" colspan="5"> 
                                Your shopping cart is empty!
                            </td>
                        </tr>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </form>

    <?php
    if (count($listcart) > 0) {
        $dataresult = GetCartResult($selectcurrency); ?>
        <b>Dealer discount:</b> <?php echo @$dealer->percent_off + 0 . "% off"; ?>
        <div class = "cart-total">
            <table id = "total">
                <tbody>

                    <tr>
                        <td class = "right"><?php echo "<del>" . DefaultCurrencyForView(@$dataresult['totalsum'], $selectcurrency) . "</del><br/>" . DefaultCurrencyForView(@$dataresult['totalsum'] - (@$dataresult['totalsum'] * (@$dealer->percent_off / 100)), $selectcurrency); ?></td>

                    </tr>
                </tbody></table>
        </div>

        <div class = "buttons">
            <div class = "right"><a href = "<?php echo base_url() . 'index.php/user/checkout' ?>" class = "button">Checkout</a></div>
            <div class = "left"><a href = "<?php echo base_url() . 'index.php/user' ?>" class = "button">Continue Shopping</a></div>
        </div>
        <?php
    } else {
        ?>
        <div class = "buttons">
            <div class = "left"><a href = "<?php echo base_url() . 'index.php/user' ?>" class = "button">Continue Shopping</a></div>
        </div> 
    <?php } ?>

</div>
<script>

    $(".btt_update").click(function () {

        $.ajax(
                {
                    url: baseurl + "/index.php/cart/updatecart",
                    data: {
                        product_id: $(this).data("id"),
                        qty: $("#txt_qty" + $(this).data("id")).val()

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            window.location.reload();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;


    })
    $(".btt_remove").click(function () {

        $.ajax(
                {
                    url: baseurl + "/index.php/cart/removecart",
                    data: {
                        product_id: $(this).data("id")

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            window.location.reload();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;


    })

    function RefreshStatus(str)
    {
        $("#coupon").css("display", "none");
        $("#voucher").css("display", "none");
        $("#shipping").css("display", "none");
        $('#' + str).css("display", "block");
    }

    $(".rd_check").change(function () {
        RefreshStatus($(this).val());
    })
    $("#dropdown_country_id").change(function () {

        $.ajax(
                {
                    url: baseurl + "/index.php/user/getState",
                    data:
                            {
                                id_country: $("#dropdown_country_id").val()
                            },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        console.log(data);
                        $("#dropdown_state_id").empty();
                        var subcat = $('<option />');
                        subcat.val(0);
                        subcat.text(' --- Please Select --- ');
                        $('#dropdown_state_id').append(subcat);
                        $.each(data, function (index, value) {
                            subcat = $('<option />');
                            subcat.val(value.kode);
                            subcat.text(value.name);
                            $('#dropdown_state_id').append(subcat);

                        });
                    },
                    error: function (xhr, status, error)
                    {

                        messageerror(xhr.responseText);
                    }
                });

    })


    $("#coupon_submit").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + "index.php/cart/add_coupon",
            data: {coupon: $("#txt_coupon").val()},
            dataType: 'json',
            success: function (data) {
                if (data.st) {
                    messagesuccess(data.msg);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                } else {
                    messagesuccess(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

    $("#coupon_remove").click(function () {
        $.ajax({
            type: 'POST',
            url: baseurl + "index.php/cart/remove_coupon",
            data: {coupon: ""},
            dataType: 'json',
            success: function (data) {
                if (data.st) {
                    messagesuccess(data.msg);
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                } else {
                    messagesuccess(data.msg);
                }
            },
            error: function (xhr, status, error) {
                messageerror(xhr.responseText);
            }
        });
        return false;
    });

</script>