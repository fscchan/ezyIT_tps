<?php
left_account("order");
$objmodel = array();
$config = GetConfig();
$selectcurrency = SelectedCurrency();
if (GetUserId() > 0) {
    $objmodel = GetMemberData();
}
?><div id="content">  
    <div class="breadcrumb">
        <div class="breadcrumb">
            <a href="<?php echo base_url() ?>">Home</a>
            » <a href="<?php echo base_url() . 'index.php/tools/account' ?>">Account</a>
            » Order History
        </div>
    </div>
    <h1>Order History</h1>

    <?php
    if (count(@$listorder) == 0) {
        echo '<br/>You have not made any previous orders!<br/><br/>';
    } else {
        ?>
        <div class="wishlist-info">
            <table>
                <thead>
                    <tr>
                        <td class="image">Image</td>
                        <td class="name">Product Name</td>
                        <td class="model">Model</td>
                        <td class="stock">Stock</td>
                        <td class="price">Unit Price</td>
                        <td class="action">Action</td>
                    </tr>
                </thead>
                <tbody id="wishlist-row1030">
                    <?php foreach ($listwhislist as $whis) { ?>
                        <tr>
                            <td class="image">            
                                <a href="<?php echo base_url() . 'index.php/user/view_product/' . $whis->product_id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $whis->product_name) . '.html' ?>">
                                    <img style="width:50px;" src="<?php echo $config['folderproduct'] . $whis->product_image ?>" alt="<?php echo $whis->product_name ?>"></a>
                            </td>
                            <td class="name">
                                <a href="<?php echo base_url() . 'index.php/user/view_product/' . $whis->product_id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $whis->product_name) . '.html' ?>">
                                    <?php echo $whis->product_name ?></a></td>
                            <td class="model"><?php echo $whis->code_generated ?></td>
                            <td class="stock"> 
                                <?php if (CheckEmpty($whis->listprice['stock'])) { ?>
                                    In Stock
                                    <?php
                                } else {
                                    ?>Out Stock<?php } ?></td>
                            <td class="price">            
                                <div class="price">
                                    <?php
                                    if (count($whis->listprice) > 0) {
                                        if ($whis->listprice['price'] > $whis->listprice['discountprice']) {
                                            ?>
                                            <span class="price-old"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $whis->listprice['price']), $selectcurrency) ?></span>
                                            <?php
                                        }
                                        ?>
                                        <span class="price-new"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $whis->listprice['discountprice']), $selectcurrency) ?></span>  
                                    <?php } else {
                                        ?>

                                        <span class="price-new"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, $whis->product_unit_cost), $selectcurrency) ?></span>          

                                    <?php } ?>                         
                                </div>
                            </td>
                            <td class="action"><a class="bttwhishaddtocart" data-id="<?php echo $whis->product_id ?>" href="javascript:;"><img src="<?php echo base_url() ?>images/cart-add.png" alt="Add to Cart" title="Add to Cart" ></a>&nbsp;&nbsp;<a class="bttwhishdelete" data-id="<?php echo $whis->product_id ?>" href="javascript:;"><img src="<?php echo base_url() ?>images/remove.png" alt="Remove" title="Remove"></a></td>
                        </tr>


                    <?php } ?>

                </tbody>
            </table>
        </div>

    <?php } ?>
</div>

<script>
    $(".bttwhishaddtocart").click(function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/cart/addtocart",
                    data: {
                        product_id: $(this).data("id")

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            modaldialog(data.msg);
                            messagesuccess(data.msg);
                            RefreshCart();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;


    })
    $(".bttwhishdelete").click(function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/tools/deletewhishlist",
                    data: {
                        product_id: $(this).data("id")

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            window.location.reload();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;



    })

</script>