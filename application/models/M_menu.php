<?php

class M_menu extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function GetOneHeaderSub($islengkap = false) {
        $this->db->from("tbl_ecommerce_sub_header_category");
        $this->db->where(array("sub_header_category_id" => $islengkap));
        $row = $this->db->get()->row();

        return $row;
    }

    function GetAllMenu($islengkap = false, $isecommerce = true) {
        $this->db->from("tbl_inv_category");
        $this->db->where(array("display_in !=" => '1'));
        $where = array();
        if ($isecommerce) {

            $where['display_in !='] = 3;
        } else {
            $where['display_in !='] = 2;
        }
        $this->db->where($where);
        $this->db->order_by("urut");
        $listmenu = $this->db->get()->result();
        if ($islengkap) {
            foreach ($listmenu as $menu) {
                $menu->submenu = $this->GetAllSubMenu($menu->cat_id);
            }
        }
        return $listmenu;
    }

    function GetLeftMenu($id_cat = 0, $isecommerce = true) {
        $this->db->from("tbl_inv_category");
        $this->db->where(array("display_in !=" => '1'));
        $this->db->order_by("urut");
        $where = array();
        if ($isecommerce) {

            $where['display_in !='] = 3;
        } else {
            $where['display_in !='] = 2;
        }
        $listmenu = $this->db->get()->result();
        foreach ($listmenu as $menu) {
            if ($menu->cat_id == $id_cat)
                $menu->submenu = $this->GetAllSubMenu($menu->cat_id);
            else
                $menu->submenu = array();
        }
        return $listmenu;
    }

    function GetOneMenu($id_cat = 0, $ikut = false) {
        $this->db->from("tbl_inv_category");
        $this->db->where(array("cat_id" => $id_cat));
        $objectmenu = $this->db->get()->row();
        if ($objectmenu != null && $ikut) {
            $objectmenu->submenu = $this->GetAllSubMenu($objectmenu->cat_id);
        }

        return $objectmenu;
    }

    function GetOneSubMenu($id_sub_cat = 0) {
        $this->db->from("tbl_inv_category_sub");
        $this->db->where(array("id_sub_category" => $id_sub_cat));
        $objectmenu = $this->db->get()->row();

        return $objectmenu;
    }

    function GetAllSubMenu($id_cat = 0, $isecommerce = true) {
        $this->db->from("tbl_inv_category_sub");
        $this->db->order_by("urut");
        if (!CheckEmpty($id_cat)) {
            $this->db->where(array("cat_id" => $id_cat, "sub_header_category_id" => 0));
        }
        $listsubmenu = $this->db->get()->result();
        $this->db->from("tbl_ecommerce_sub_header_category");
        if (!CheckEmpty($id_cat)) {
            $this->db->where(array("cat_id" => $id_cat));
        }
        $this->db->select("sub_header_category_id as id_sub_category,sub_header_category_name as name_sub_category");
        $listsubheading = $this->db->get()->result();

        foreach ($listsubheading as $heading) {
            $this->db->from("tbl_inv_category_sub");
            $this->db->order_by("urut");
            $this->db->where(array("sub_header_category_id" => $heading->id_sub_category));
            $heading->listsubsubmenu = $this->db->get()->result();
            $countheading = 0;
            foreach ($heading->listsubsubmenu as $menu) {
                $menu->jenis = 'cat';
                $this->db->from("tbl_inv_product_master");
                $this->db->where(array("product_status !=" => '1'));
                $where = array();

                $where['product_active'] = 1;
                $where['product_sub_category_id'] = $menu->id_sub_category;
                if ($isecommerce) {
                    $where['product_status !='] = 3;
                } else {
                    $where['product_status !='] = 2;
                }
                $this->db->where($where);
                $menu->countitem = $this->db->get()->num_rows();
                $countheading+=$menu->countitem;
            }

            $heading->jenis = 'heading';
            $heading->countitem = $countheading;
        }

        foreach ($listsubmenu as $menu) {
            $menu->listsubsubmenu = array();
            $menu->jenis = 'cat';
            $this->db->from("tbl_inv_product_master");
            $this->db->where(array("product_status !=" => '1'));
            $where = array();

            $where['product_active'] = 1;
            $where['product_sub_category_id'] = $menu->id_sub_category;
            if ($isecommerce) {
                $where['product_status !='] = 3;
            } else {
                $where['product_status !='] = 2;
            }
            $this->db->where($where);
            $menu->countitem = $this->db->get()->num_rows();
        }

        $listsubmenu = array_merge((array) $listsubmenu, (array) $listsubheading);
        usort($listsubmenu, function($a, $b) {
            return strcmp($a->name_sub_category, $b->name_sub_category);
        });

        //$listsubmenu=  array_msort($listsubmenu, 'name_sub_category');
        $listsubmenu = (object) $listsubmenu;
        return $listsubmenu;
    }

    function get_categories() {
        $query = $this->db->get('tbl_ecommerce_cattegory_article');
        $return = array();

        foreach ($query->result() as $category) {
            $return[$category->id_category] = $category;
            $return[$category->id_category]->subs = $this->get_sub_categories($category->id_category); // Get the categories sub categories
        }

        return $return;
    }

    public function get_sub_categories($id_category) {
        $this->db->where('id_category', $id_category);
        $query = $this->db->get('tbl_ecommerce_articles');
        return $query->result();
    }

}

?>