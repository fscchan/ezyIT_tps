<?php

class User extends CI_Controller {

    public function subscribber() {
        $salutation = $this->input->post("salutation");
        $name = $this->input->post("name");
        $email = $this->input->post("email");
    }
    public function view_article($id) {
        $model = array();
        $model['title'] = 'Home';
        $model['cat_id'] = $id;
        $this->load->model("m_article");
        $this->load->model("m_menu");
        $javascript = array();
        //$model['cat_id'] = $id; 
        $model['article'] = $this->m_article->GetOneArticle($id);
        LoadTemplate($model, 'user/varticle', $javascript);
    }

    public function forgot_action() {
        $message = '';
        $data = array();
        $model = $this->input->post();
        $data['st'] = false;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->load->model("user/m_user");
        $this->load->model("m_email");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->forgot_password($model['email']);
            if ($message == '') {
                $this->m_email->SendEmailRecovery($model['email']);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message != '' ? $message : "We have sent you email step for recover your password<br/>";
        }
        echo json_encode($data);
    }

    public function getState() {
        $this->load->model("user/m_area");
        $id_country = $this->input->post("id_country");
        echo json_encode($this->m_area->GetAllState($id_country));
    }

    public function cart() {
        $model = array();
        $model['isshowcart'] = 1;
        $this->load->model("user/m_area");
        $this->load->model("user/m_user");
        $model['listcountry'] = $this->m_area->GetAllCountries();
		$model["dealer"] = $this->m_user->GetUserGroup();
        LoadTemplate($model, 'user/vcart', array());
    }

    public function checkout() {
        $model = array();
        $model['isshowcart'] = 1;
        $this->load->model("user/m_area");
        $this->load->model("user/m_user");
        $model['listcountry'] = $this->m_area->GetAllCountries();
		$model["dealer"] = $this->m_user->GetUserGroup();
		LoadTemplate($model, 'user/vcheckout', array());
    }

    public function register_action() {
        $message = '';
        $data = array();
        $model = $this->input->post();
        $data['st'] = false;
        $this->form_validation->set_rules('firstname', "First Name", 'required');
        $this->form_validation->set_rules('lastname', "Last Name", 'required');
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('telephone', "Telephone", 'required');
        $this->form_validation->set_rules('fax', "Fax", 'required');
        $this->form_validation->set_rules('address', "Address", 'required');
        $this->form_validation->set_rules('city', "City", 'required');
        $this->form_validation->set_rules('country_id', "Country", 'required');
        $this->form_validation->set_rules('password', "Password", 'required|min_length[6]');
        $this->form_validation->set_rules('confirm', "Confirm Password", 'required|matches[password]');
        $this->load->model("user/m_user");
        $message = $this->m_user->validate_account($model['email']);
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->register_account($model);
            $data['st'] = $message == '';
            $data['msg'] = $message != '' ? $message : "Your account successfully registered into database we have sent you the email step for activate your account";
        }
        echo json_encode($data);
    }

    public function view_category($id) {
        $model = array();
        $this->load->library('pagination');
        $start = $this->input->get_post('per_page');
        $view = 16;
        $model['title'] = 'Home';
        $this->load->model("m_menu");
        $this->load->model("m_product");
        $model['cat_id'] = $id;
        $model['model'] = $this->m_menu->GetOneMenu($id);
        $javascript = array();
        $model['listproduct'] = $this->m_product->GetListProduct($id, 0, 0, $start, $view);
        $config['base_url'] = base_url() . 'index.php/user/view_category/' . $id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model['model']->cat_name) . '.html';
        $config['total_rows'] = $this->m_product->GetTotalProducts($id, 0, 0);
        $config['per_page'] = $view;
        $config["uri_segment"] = 1;
        $config['first_link'] = 'First';
        $config['cur_tag_open'] = '<b>';
        $config['cur_tag_close'] = '</b>';
        $config['use_page_numbers'] = TRUE;
        $page_num = $this->uri->segment(1);
        $offset = ($page_num == NULL) ? 0 :
                ($page_num * $config['per_page']) - $config['per_page'];

        $model['configpaging'] = $config;
        $this->pagination->initialize($config);
        $model['paging'] = $this->pagination->create_links();
        $model['text'] = $this->pagination->current_place();
        LoadTemplate($model, 'user/vcategory', $javascript);
    }

    public function changecurrency() {
        $currency = $this->input->post("currency");
        $currenturl = $this->input->post("currenturl");
        $url = parse_url($currenturl);
        $querystring = '';
        $listquerystring = array();
        $listurl = explode("?", $currenturl);
        $returnrul = $listurl[0];
        $this->load->model("m_currency");
        if (CheckEmpty($currency)) {
            $currency = 0;
        }


        if (count($listurl > 1)) {
            if (array_key_exists("query", $url)) {
                parse_str($url['query'], $listquerystring);
                foreach ($listquerystring as $key => $query) {

                    if ($key != "cry") {

                        $querystring.=$key . "=" . $query . "&";
                    }
                }
            }
        }

        $modelcurrency = $this->m_currency->GetOneCurrency($currency);
        if ($modelcurrency != null) {
            $this->session->set_userdata("currency", $modelcurrency->id_currency);
            if (!($modelcurrency->default_currency == "1")) {
                $querystring = "cry=" . $modelcurrency->id_currency . "&" . $querystring;
            }
        }

        if (strlen($querystring) > 0) {
            $querystring = substr($querystring, 0, strlen($querystring) - 1);
        }

        if (strlen($querystring) > 0) {
            $returnrul.="?" . $querystring;
        }

        $model = array();
        $model['st'] = true;
        $model['url'] = $returnrul;
        echo json_encode($model);
    }

    public function activate() {
        $this->input->post("email");
    }

    public function login_validation() {
        $model = $this->input->post();
        $message = '';
        $data = array();
        $data['st'] = false;
        $this->load->model('m_user');
        $this->load->model('cart/m_cart');
        $this->lang->load("user");
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('password', "Password", 'required');


        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->login($model['email'], $model['password'], false);
            $data['st'] = $message == '';
            if ($message == '') {
                if (array_key_exists('isremove', $model)) {
                    if ($model['isremove'] == 1) {
                        $this->m_cart->EmptyCartDatabase();
                    }
                }
                $this->m_cart->SyncronDbAndCart();
            }
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    /*
      public function login_validation() {
      $model = $this->input->post();
      $message = '';
      $data = array();
      $data['st'] = false;
      $config = GetConfig();
      $this->lang->load("user");
      $this->form_validation->set_rules('email', "Email", 'required');
      $this->form_validation->set_rules('password', "Password", 'required');

      if (!CheckEmpty($model['email']) && !CheckEmpty($model['password'])) {
      $arraypost = array();
      $arraypost['IC'] = $model['password'];
      $arraypost['email'] = $model['email'];

      $listuser = json_decode(GetResponseFromAPI($config['loginurl'], $arraypost));

      if (count($listuser) != 1) {
      $message.=sprintf($this->lang->line('account_login_not_find')) . '<br/>';
      } else {
      SetUserId($listuser[0]->member_id);

      $data['st'] = true;
      }
      }

      if ($this->form_validation->run() === FALSE || $message != '') {
      $data['msg'] = 'Error :' . validation_errors() . $message;
      }
      echo json_encode($data);
      exit;
      }
     */

    /* public function edit_info() {
      $model = $this->input->post();
      $arraypost = array();
      $arraypost['primary[member_id]'] = GetUserId();
      $arraypost['data[name]'] = $model['name'];
      $arraypost['data[email]'] = $model['email'];
      $arraypost['data[mobile_phone]'] = $model['mobile_phone'];
      $returnpost = '';
      $message = '';
      $data = array();
      $data['st'] = false;
      $config = GetConfig();
      $this->lang->load("user");
      $this->form_validation->set_rules('email', "Email", 'required');
      if ($this->form_validation->run() === FALSE || $message != '') {
      $data['msg'] = 'Error :' . validation_errors() . $message;
      } else {
      $returnpost = json_decode(GetResponseFromAPI($config['loginurl'], array(), 'PUT', $arraypost));
      if ($returnpost->message == "Success") {
      $data['st'] = true;
      } else {
      $data['msg'] = "No update for your account";
      }
      }
      echo json_encode($data);
      exit;
      } */

    public function forgot() {
        LoadTemplate(array(), 'user/vforgot', array());
    }

    public function message() {
        if (GetMessage() == '' || GetMessageStatus() == 5) {
            redirect(base_url());
        }
        LoadTemplate(array(), 'user/vmessage', array());
    }

    public function edit_info() {
        $model = $this->input->post();

        $arraypost = array();
        $returnpost = '';
        $message = '';
        $data = array();
        $data['st'] = false;
        $config = GetConfig();
        $this->lang->load("user");
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('first_name', "First Name", 'required');
        $this->form_validation->set_rules('country_id', "Country", 'required');

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if ($model['member_id'] != GetUserId()) {
                $message.='This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $this->load->model('user/m_user');
                $this->m_user->edit_info($model);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    public function logout() {
        SetUserId(0);
        redirect(base_url() . 'index.php/user/login');
    }

    public function login() {
        if (GetUserId() > 0) {
            redirect(base_url());
        }
        LoadTemplate(array(), 'user/vlogin_user', array());
    }

    public function register() {
        if (GetUserId() > 0) {
            redirect(base_url());
        }
        $model = array();
        $this->load->model("user/m_area");
        $model['listcountry'] = $this->m_area->GetAllCountries();
        LoadTemplate($model, 'user/vregister', array());
    }

    public function view_category_discount($id) {
        $model = array();
        $model['title'] = 'Home';
        $this->load->model("m_discount");
        $this->load->model("m_product");
        $model['model'] = $this->m_discount->GetOneDiscount($id);

        $this->load->library('pagination');
        $start = $this->input->get_post('per_page');
        $view = 16;

        if (!CheckEmpty($model['model'])) {
            $model['title'] = $model['model']->nama_discount;
        }
        $model['listproduct'] = $this->m_discount->GetListDiscount($id, $start, $view);
        $config['base_url'] = base_url() . 'index.php/user/view_category_discount/' . $id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model['model']->nama_discount) . '.html';
        $config['total_rows'] = $this->m_discount->GetTotalDiscount($id);
        $config['per_page'] = $view;
        $config["uri_segment"] = 1;
        $config['first_link'] = 'First';
        $config['cur_tag_open'] = '<b>';
        $config['cur_tag_close'] = '</b>';
        $config['use_page_numbers'] = TRUE;
        $page_num = $this->uri->segment(1);

        $offset = ($page_num == NULL) ? 0 :
                ($page_num * $config['per_page']) - $config['per_page'];

        $model['configpaging'] = $config;
        $this->pagination->initialize($config);
        $model['paging'] = $this->pagination->create_links();
        $model['text'] = $this->pagination->current_place();
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'user/vdiscount', $javascript);
    }

    public function view_product($id = 0) {
        $model = array();
        $this->load->model("m_product");
        $product = $this->m_product->GetOneProduct($id);
        $this->load->model("m_discount");

        $model['model'] = $product;
        if ($product != null) {
            $model['cat_id'] = $product->product_category;
            $product->listprice = $this->m_discount->GetDiscountItem($product->product_id);
            $model['sub_cat_id'] = $product->product_sub_category_id;
			$model['min_stock'] = $product->product_min_stock;
        }

        LoadTemplate($model, 'product/vproduct', array());
    }

    public function subscribe() {
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('name', "Name", 'required');
        $message = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
            $data['st'] = false;
        } else {
            $message = $this->m_user->Subscribe($this->input->post());
            $data['st'] = $message == '';
            $data['msg'] = ($message == '' ? 'Thanks for subscribe to ThePaperstone , we will inform you any update about product and information in ThePaperstone' : $message);
        }
        echo json_encode($data);
    }

    public function view_header_sub_category($id) {
        $model = array();
        $model['title'] = 'Home';
        $this->load->model("m_menu");
        $this->load->model("m_product");
        $model['headerid'] = $id;
        $model['model'] = $this->m_menu->GetOneHeaderSub($id);
 
        $this->load->library('pagination');
        $start = $this->input->get_post('per_page');
        $view = 16;

        if (!CheckEmpty($model['model'])) {
            $model['model']->parent = $this->m_menu->GetOneMenu($model['model']->cat_id, false);
            $model['cat_id'] = $model['model']->cat_id;

            $model['title'] = $model['model']->sub_header_category_name;
        }
        $model['listproduct'] = $this->m_product->GetListProduct(0, 0, $id, $start, $view, $id);
        $config['base_url'] = base_url() . 'index.php/user/view_header_sub_category/' . $id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model['model']->sub_header_category_name) . '.html';
        $config['total_rows'] = $this->m_product->GetTotalProducts(0, 0, $id);
        $config['per_page'] = $view;
        $config["uri_segment"] = 1;
        $config['first_link'] = 'First';
        $config['cur_tag_open'] = '<b>';
        $config['cur_tag_close'] = '</b>';
        $config['use_page_numbers'] = TRUE;
        $page_num = $this->uri->segment(1);
        $offset = ($page_num == NULL) ? 0 :
                ($page_num * $config['per_page']) - $config['per_page'];

        $model['configpaging'] = $config;
        $this->pagination->initialize($config);
        $model['paging'] = $this->pagination->create_links();
        $model['text'] = $this->pagination->current_place();
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'user/vheadersub', $javascript);
    }

    public function view_sub_category($id) {
        $model = array();
        $model['title'] = 'Home';
        $this->load->model("m_menu");
        $this->load->model("m_product");
        $model['sub_cat_id'] = $id;
        $model['model'] = $this->m_menu->GetOneSubMenu($id);

        $this->load->library('pagination');
        $start = $this->input->get_post('per_page');
        $view = 16;

        if (!CheckEmpty($model['model'])) {
            $model['model']->parent = $this->m_menu->GetOneMenu($model['model']->cat_id, false);
            $model['cat_id'] = $model['model']->cat_id;
            $model['headerid'] = $model['model']->sub_header_category_id;
            $model['title'] = $model['model']->name_sub_category;
        }
        $model['listproduct'] = $this->m_product->GetListProduct(0, $id, 0, $start, $view);
        $config['base_url'] = base_url() . 'index.php/user/view_sub_category/' . $id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model['model']->name_sub_category) . '.html';
        $config['total_rows'] = $this->m_product->GetTotalProducts(0, $id, 0);
        $config['per_page'] = $view;
        $config["uri_segment"] = 1;
        $config['first_link'] = 'First';
        $config['cur_tag_open'] = '<b>';
        $config['cur_tag_close'] = '</b>';
        $config['use_page_numbers'] = TRUE;
        $page_num = $this->uri->segment(1);
        $offset = ($page_num == NULL) ? 0 :
                ($page_num * $config['per_page']) - $config['per_page'];

        $model['configpaging'] = $config;
        $this->pagination->initialize($config);
        $model['paging'] = $this->pagination->create_links();
        $model['text'] = $this->pagination->current_place();
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'user/vcategorysub', $javascript);
    }
	
	public function topup() {
		/* $membership = GetMembership();
		if (empty($membership)) {
			redirect(base_url() . "index.php/user/login", "location");
		} else {
			LoadTemplate(array(), "user/vtopup", array());
		} */
		LoadTemplate(array(), "user/vtopup", array());
	}

	public function register_premium() {
		/* $membership = GetMembership();
		if (empty($membership)) {
			redirect(base_url() . "index.php/user/login", "location");
		} else {
			LoadTemplate(array(), "user/vpregister", array());
		} */
		LoadTemplate(array(), "user/vpregister", array());
	}
	
	public function preg() {
		/* $mstats = GetMembership(); */
		$config = GetConfig();
		// $items = json_decode(GetResponseFromAPI($config["membermenuurl"], array()), true); // registration using this causes paypal api to fail since price = null
		$items = json_decode('[{"id": "1","type": "New Membership $55","commission_amount": "0.50","credit_amount": 55.00,"price": 55.00,"status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "2","type": "New Membership $107","commission_amount": "1.00","credit_amount": 115.00,"price": 107.00,"status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "3","type": "New Membership $500","commission_amount": "5.00","credit_amount": 550.00,"price": 500.00,"status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "4","type": "New Temporary Membership","commission_amount": "0.10","credit_amount": null,"price": null,"status": "1","create_date": null,"create_by": null,"edit_date": "2016-09-02 14:31:00","edit_by": null},{"id": "5","type": "Top Up - $25","commission_amount": "0.22","credit_amount": "25.00","price": "25.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-30 15:11:01","edit_by": "1"},{"id": "6","type": "Top Up - $55","commission_amount": "0.55","credit_amount": "58.00","price": "55.00","status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "7","type": "Top Up - $115","commission_amount": "1.00","credit_amount": "115.00","price": "107.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-22 10:11:57","edit_by": "1"},{"id": "8","type": "Top Up - $500","commission_amount": "5.00","credit_amount": "550.00","price": "500.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-22 10:11:57","edit_by": "1"}]', true);
		$item = $items[$this->input->post("param") - 1];
		// var_dump($item);
		$now = date("Y-m-d H:i:s");
		$datareturn = array();
		$datareturn["jns"] = "paypal";
		$datareturn["account_paypal"] = $config["isproduction"] == "1" ? $config["paypalsandbox"] : $config["paypalproduction"];
		if ($config["isproduction"] != "1") {
			$datareturn["action"] = "https://www.paypal.com/cgi-bin/webscr";
		} else {
			$datareturn["action"] = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		}
		$datareturn["amount"] = $item["price"];
		$datareturn["cpp_header_image"] = base_url() . "images/logopaypal.png";
		$datapaypal = array(
			"METHOD" => "SetExpressCheckout",
			"REQCONFIRMSHIPPING" => 0,
			"LOCALECODE" => "EN",
			"LANDINGPAGE" => "Login",
			"CHANNELTYPE" => "Merchant",
			"PAYMENTREQUEST_0_CURRENCYCODE" => "SGD",
			"PAYMENTREQUEST_0_AMT" => $item["price"]
		);
		if (preg_match("/^(New)/i", $item["type"])) {
			$ivtype = "PM-" . date("mY");
			$query = $this->db->query('select ifnull(max(right(order_number,4)),0) as "max" from tbl_ecommerce_pmembership where order_number like "' . $ivtype . '%"');
			$lastinvoice = $query->row()->max;
			$invoice = $ivtype . str_pad($lastinvoice + 1, "5", "0", STR_PAD_LEFT);
			$datareturn["return"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datareturn["custom"] = $invoice;
			$datareturn["complete"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datareturn["st"] = true;
			$datapaypal["RETURNURL"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datapaypal["CANCELURL"] = base_url() . "index.php/tools/canceltrans/" . $invoice;
			$this->load->model("m_paypal");
			$result = $this->m_paypal->call($datapaypal);
			// var_dump($result);
			if (!CheckKey($result, "TOKEN")) {
				if (CheckKey($result, "L_LONGMESSAGE0")) {
					SetMessageSession(0, $result["L_LONGMESSAGE0"]);
				} else {
					SetMessageSession(0, "Some Error Occured");
				}
				// this->load->model("m_order");
				// $this->ReturnBackOrder($invoice);
				$paypal["url"] = base_url() . "index/php/user/message";
			} else {
				$this->session->set_userdata("paypaltoken", $result["TOKEN"]);
			}

			if ($config["isproduction"] == 1) {
				$paypal["st"] = true;
				$paypal["url"] = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" . $result["TOKEN"];
			} else {
				$paypal["st"] = true;
				$paypal["url"] = "https://www.paypal.com/cgi-bin/webscr?cmd=express-checkout&token=" . $result["TOKEN"];
			}
			/* $user = json_decode(json_encode(GetMemberData()), true);
			$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			$insert["membership_card_no"] = "";
			$insert["type"] = "1";
			$insert["valid_date"] = null;
			$insert["name"] = $user["first_name"] . " " . $user["last_name"];
			$insert["password"] = $user["password"];
			$insert["IC"] = "";
			$insert["credits"] = 0;
			$insert["email"] = $user["email"];
			$insert["birth_month"] = "";
			$insert["mobile_phone"] = $user["telephone"];
			$insert["use_password"] = "off";
			$insert["location_id"] = "";
			$insert["status"] = 1;
			$insert["create_date"] = $now;
			$insert["create_by"] = 0;
			$insert["edit_date"] = $now;
			$insert["edit_by"] = 1;
			$insert["closure_date"] = "0000-00-00 00:00:00";
			$insert["receipt_number"] = $invoice;
			$response = json_decode(GetResponseFromAPI($config["loginurl"], array(), "POST", $insert), true); */
		} elseif (preg_match("/^(Top)/i", $item["type"])) {
			$ivtype = "TP-" . date("mY");
			$query = $this->db->query('select ifnull(max(right(order_number,4)),0) as "max" from tbl_ecommerce_pmembership where order_number like "' . $ivtype . '%"');
			$lastinvoice = $query->row()->max;
			$invoice = $ivtype . str_pad($lastinvoice + 1, "5", "0", STR_PAD_LEFT);
			$datareturn["return"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datareturn["custom"] = $invoice;
			$datareturn["complete"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datareturn["st"] = true;
			$datapaypal["RETURNURL"] = base_url() . "index.php/tools/checktrans/" . $invoice;
			$datapaypal["CANCELURL"] = base_url() . "index.php/tools/canceltrans/" . $invoice;
			$this->load->model("m_paypal");
			$result = $this->m_paypal->call($datapaypal);
			if (!CheckKey($result, "TOKEN")) {
				if (CheckKey($result, "L_LONGMESSAGE0")) {
					SetMessageSession(0, $result["L_LONGMESSAGE0"]);
				} else {
					SetMessageSession(0, "Some Error Occured");
				}
				// this->load->model("m_order");
				// $this->ReturnBackOrder($invoice);
				$paypal["url"] = base_url() . "index/php/user/message";
			}
			else {
				$this->session->set_userdata("paypaltoken", $result["TOKEN"]);
			}

			if ($config["isproduction"] == 1) {
				$paypal["st"] = true;
				$paypal["url"] = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=" . $result["TOKEN"];
			} else {
				$paypal["st"] = true;
				$paypal["url"] = "https://www.paypal.com/cgi-bin/webscr?cmd=express-checkout&token=" . $result["TOKEN"];
			}
			/* $user = GetMemberData();
			$oldmember = json_decode(GetResponseFromAPI($config["loginurl"], array("email" => $user->email)), true);
			$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			$update["credits"] = $oldmember["credits"] + $item["credit_amount"];
			$update["edit_date"] = $now;
			$update["receipt_number"] = $invoice;
			$response = json_decode(GetResponseFromAPI($config["loginurl"], array("data"=>array("data"=>array("email"=>$user["email"]))), "PUT", $update), true); */
		} else {
			die(json_encode(array("st" => false, "msg" => "Invalid parameters")));
		}
		$order["order_number"] = $invoice;
		$order["dateorder"] = date("Y-m-d");
		$order["id_customer_ecommerce"] = GetUserId();
		$order["status_order"] = 0;
		$order["item_number"] = $item["id"];
		$order["totalamount"] = $item["price"];
		$order["paymenttype"] = "pp_express";
		$order["coupon"] = 0;
		$order["grandtotal"] = $item["price"];
		$order["gst"] = $item["price"] * 10 / 100;
		$order["create_date"] = $now;
		$order["create_by"] = GetUserId();
		$this->db->insert("tbl_ecommerce_pmembership", $order);
		$this->db->from("tbl_ecommerce_pmembership");
		$this->db->where(array("id_order" => $this->db->insert_id()));
		$detail = json_decode(json_encode($this->db->get()->row()), true);
		$detail["humandate"] = date_format(date_create_from_format("Y-m-d H:i:s", $detail["create_date"]), "d F Y H:i:s");
		die(json_encode(array("st" => "true", "msg" => "Your order is ready - " . (preg_match("/^PM-/", $detail["order_number"]) ? "Registration" : "Top-up"), "details" => $detail, "datareturns" => $datareturn, "results" => $result, "paypal" => $paypal)));
	}
}