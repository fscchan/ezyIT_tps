<?php
class M_coupon extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function GetAllAffiliate() {
        $this->db->from("tb_affiliate");
        $this->db->select("*");
        $list = $this->db->get()->result();

        return $list;
    }

   
    function GetHistoryAffiliate($model){
        $this->db->from("tb_affiliate_history");
        $where=array();
        if(!CheckEmpty($model['startdate']))
        $where['createdate >=']=  DefaultTanggalDatabase($model['startdate']);
          if(!CheckEmpty($model['enddate']))
        $where['createdate <=']=  DefaultTanggalDatabase($model['enddate']);
        $this->db->order_by('createdate');
        $this->db->where($where);
        $listhistory= $this->db->get()->result();
        
        return $listhistory;
    }
     function GetHistoryAffiliateTotal($model){
        $this->db->from("tb_affiliate_history");
        $where=array();
        if(!CheckEmpty($model['startdate']))
        $where['createdate >=']=  DefaultTanggalDatabase($model['startdate']);
          if(!CheckEmpty($model['enddate']))
        $where['createdate <=']=  DefaultTanggalDatabase($model['enddate']);
        $this->db->select("urlwebsite,codeurl,count(*)as total");
        $this->db->group_by(array("codeurl","urlwebsite"));
          
        $this->db->where($where);
        $listhistory= $this->db->get()->result();
        
        return $listhistory;
    }
    function validateurl($model) {
        $this->db->from("tb_affiliate");
        $this->db->where(array("status" => 1, "url_code" => $model['code']));
        $row = $this->db->get()->row();
      
        if ($row != null) {
            $history = array();
           

            $history['fromurl'] = $model['urlsebelumnya'];
            $history['codeurl'] = $model['code'];
            $history['createdate'] = GetDateNow();
            $history['urlwebsite'] = $row->url_website;
            $history['ip']=$model['userip'];
            $this->db->insert("tb_affiliate_history",$history);
        }
    }

    function GetOneAffiliate($id) {
        $this->db->from("tb_affiliate");
        return $this->db->where(array("id_affiliate" => $id))->get()->row();
    }

    function affiliate_delete($id) {
        $this->db->delete("tb_affiliate", array("id_affiliate" => $id));
    }

    function affiliate_create($model) {
        if (CheckEmpty($model['id_affiliate'])) {
            if (CheckEmpty($model['url_code'])) {
                $voucherserial = '';
                do {
                    $voucherserial = RandomVoucher();
                } while ($this->db->query('select * from tb_affiliate where url_code="' . $voucherserial . '" ')->num_rows !== 0);
                $model['url_code'] = $voucherserial;
            }
            $this->db->insert("tb_affiliate", $model);
        } else {
            $this->db->update("tb_affiliate", $model, array("id_affiliate" => $model['id_affiliate']));
        }
    }

    Function IsInCoupon($serial, $id_product) {
        $coupon = $this->GetOneCouponFromSerial($serial);
        $canuse = false;
        if (count($coupon->listofferdetail) > 0 || count($coupon->listcategory) > 0) {

            foreach ($coupon->listofferdetail as $offer) {

                if ($id_product == $offer->id_products) {
                    $canuse = true;
                }
            }

            foreach ($coupon->listcategory as $category) {

                $listcategory = $this->m_product->GetCategoryIdFromProduct($id_product);
                foreach ($listcategory as $categor) {

                    if ($categor['id_category'] == $category['id_category']) {
                        $canuse = true;
                    }
                }
            }
        } else {
            $canuse = true;
        }
        return $canuse;
    }

    Function GetOneCouponFromSerial($serial, $checkuse = false) {
        $usecount = 0;
        if ($checkuse === true && GetUserId() !== 0) {
            $row = $this->db->query("SELECT count(`coupon`) as `usecount` FROM `tbl_ecommerce_order` WHERE `id_customer_ecommerce` = '" . GetUserId() . "' AND `coupon` = " . $this->db->escape($this->input->post('coupon')));
            $usecount = $row->usecount;
        }
        $this->db->from("tbl_inv_coupon");
        $this->db->where(array("coup_serial" => $serial));
        $row = $this->db->get()->row();
       
        if ($row != null && $row->coup_isactive != "0") {
            if (($row->coup_by_date == 1 && $row->coup_end_publishing != null && date("Y-m-d H:i:s") < $row->coup_end_publishing)||$row->coup_by_date==2) {
                
                return array("coupon_id" => $row->coup_id, "coupon_serial" => $row->coup_serial, "coupon_type" => $row->coup_by_date, "coupon_value" => $row->coup_value, "coupon_use" => $usecount, "coupon_status" => $row->coup_isactive, "coupon_minimum" => $row->coup_min_price);
            } else {
                
                return null;
            }
        } else {
            return null;
        }
    }

    Function GetOneCoupon($id) {
        $this->db->from("tb_coupon");
        $this->db->where(array("id_coupon" => $id));
        $row = $this->db->get()->row();
        if ($row != null) {
            $this->db->from("tb_coupon_products");
            $this->db->where(array("id_coupon" => $id));
            $this->db->select("id_products");
            $row->listofferdetail = $this->db->get()->result();
            $this->db->from("tb_coupon_category");
            $this->db->where(array("id_coupon" => $id));
            $this->db->select("id_category");
            $row->listcategory = $this->db->get()->result_array();
        }


        return $row;
    }

    Function GetAllCoupon() {
        $this->db->from("tb_coupon");
        $this->db->select("*");
        $this->db->order_by('id_coupon', "desc");
        return $this->db->get()->result();
    }
     function GetDiscountFromList($listcoupon,$id_product, $qty) {
        $this->db->from("tb_products");
        $this->db->select("*");
        $this->db->where(array("id_products" => $id_product));
        $row = $this->db->get()->row();
        
        $discount = 0;
        if ($row != null) {
            foreach ($listcoupon as $coupon) {
                if ($this->IsInCoupon($coupon, $id_product)) {
                    $this->db->from("tb_coupon");
                    $this->db->where(array("serial_coupon" => $coupon));
                    $couponobject = $this->db->get()->row();
                    if ($couponobject != null) {
                        if ($couponobject->jenis_discount == 0) {
                            if ($couponobject->value > 100) {
                                $discount = $row->price;
                            } else {
                                $discount = ($row->price * $couponobject->value / 100);
                            }
                            $discount = $discount * $qty;
                        } else {
                            if ($couponobject->refer_to == "1") {
                                if ($couponobject->value > $row->price) {
                                    $discount = $row->price * $qty;
                                } else {
                                    $discount = $couponobject->value * $qty;
                                }
                            } else {
                                if ($row->price * $qty < $couponobject->value) {
                                    $discount = $row->price * $qty;
                                } else {
                                    $discount = $couponobject->value;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return $discount;
    }
    function GetDiscount($id_product, $qty) {
        $this->db->from("tb_products");
        $this->db->select("*");
        $this->db->where(array("id_products" => $id_product));
        $row = $this->db->get()->row();
        $discount = 0;
        if ($row != null) {
            foreach (GetCoupon() as $coupon) {
                if ($this->IsInCoupon($coupon['serial'], $id_product)) {
                    $this->db->from("tb_coupon");
                    $this->db->where(array("serial_coupon" => $coupon['serial']));
                    $couponobject = $this->db->get()->row();
                    if ($couponobject != null) {
                        if ($couponobject->jenis_discount == 0) {
                            if ($couponobject->value > 100) {
                                $discount = $row->price;
                            } else {
                                $discount = ($row->price * $couponobject->value / 100);
                            }
                            $discount = $discount * $qty;
                        } else {
                            if ($couponobject->refer_to == "1") {
                                if ($couponobject->value > $row->price) {
                                    $discount = $row->price * $qty;
                                } else {
                                    $discount = $couponobject->value * $qty;
                                }
                            } else {
                                if ($row->price * $qty < $couponobject->value) {
                                    $discount = $row->price * $qty;
                                } else {
                                    $discount = $couponobject->value;
                                }
                            }
                        }
                        break;
                    }
                }
            }
        }
        return $discount;
    }

    function coupon_delete($id_coupon) {

        $this->db->delete('tb_coupon_products', array("id_coupon" => $id_coupon));
        $this->db->delete('tb_coupon_category', array("id_coupon" => $id_coupon));
        $this->db->delete('tb_coupon', array("id_coupon" => $id_coupon));
    }

    Function coupon_create($model) {
        $coupon = (object) array();

        $coupon->id_coupon = $model['id_coupon'];

        $coupon->by_date = $model['by_date'];
        $coupon->end_publishing = DefaultTanggalDatabase($model['end_publishing']);
        $coupon->name_coupon = $model['name_coupon'];

        $coupon->isactive = $model['isactive'];
        $coupon->jenis_discount = $model['jenis_discount'];
        $coupon->value = $model['value'];
        $coupon->refer_to = $model['refer_to'];
        $listofferdetail = CheckArray($model, 'listofferdetail');
        $listcategory = CheckArray($model, 'listcategory');
        if (CheckEmpty($model['id_coupon'])) {
            $coupon->serial_coupon = $model['serial_coupon'];
            if (CheckEmpty($model['serial_coupon'])) {
                $voucherserial = '';
                do {
                    $voucherserial = RandomVoucher();
                } while ($this->db->query('select * from tb_coupon where serial_coupon="' . $voucherserial . '" ')->num_rows !== 0);
                $coupon->serial_coupon = $voucherserial;
            }
            $this->db->insert('tb_coupon', $coupon);
            $id_coupon = $this->db->insert_id();
            foreach ($listofferdetail as $offerdetail) {
                $this->db->insert('tb_coupon_products', array("id_coupon" => $id_coupon, "id_products" => $offerdetail['id_products']));
            }
            foreach ($listcategory as $category) {
                $this->db->insert('tb_coupon_category', array("id_coupon" => $id_coupon, "id_category" => $category));
            }
            $this->session->set_userdata('message', 'Data Coupon succesfully add to database <br/>');
            $this->session->set_userdata('messagestatus', '1');
        } else {
            if($model['serial_coupon']!="")
            {
                $coupon->serial_coupon = $model['serial_coupon'];
            }
            $this->db->update('tb_coupon', $coupon, array("id_coupon" => $model['id_coupon']));
            $strcoupon = "";

            foreach ((array) $listofferdetail as $offerdetail) {
                $this->db->from("tb_coupon_products");
                $this->db->select("*");

                $this->db->where(array("id_products" => $offerdetail['id_products'], "id_coupon" => $model['id_coupon']));
                $tempcolpoint = $this->db->get()->row();
                if ($tempcolpoint == null) {
                    $this->db->insert('tb_coupon_products', array("id_coupon" => $model['id_coupon'], "id_products" => $offerdetail['id_products']));
                }

                $strcoupon.=$offerdetail['id_products'] . ' ,';
            }
            if (count($listofferdetail) == 0) {
                $this->db->delete("tb_coupon_products", array("id_coupon" => $model['id_coupon']));
            } else {
                if (strlen($strcoupon) > 0) {
                    $strcoupon = substr($strcoupon, 0, strlen($strcoupon) - 1);
                    $strcoupon = 'Delete from tb_coupon_products where id_coupon=' . $model['id_coupon'] . ' and id_products not in (' . $strcoupon . ')';
                    $query = $this->db->query($strcoupon);
                }
            }
            $strcoupon = '';
            foreach ((array) $listcategory as $category) {
                $this->db->from("tb_coupon_category");
                $this->db->select("*");

                $this->db->where(array("id_category" => $category, "id_coupon" => $model['id_coupon']));
                $tempcolpoint = $this->db->get()->row();
                if ($tempcolpoint == null) {
                    $this->db->insert('tb_coupon_category', array("id_coupon" => $model['id_coupon'], "id_category" => $category));
                }

                $strcoupon.=$category . ' ,';
            }
            if (count($listcategory) == 0) {
                $this->db->delete("tb_coupon_category", array("id_coupon" => $model['id_coupon']));
            } else {
                if (strlen($strcoupon) > 0) {
                    $strcoupon = substr($strcoupon, 0, strlen($strcoupon) - 1);
                    $strcoupon = 'Delete from tb_coupon_category where id_coupon=' . $model['id_coupon'] . ' and id_category not in (' . $strcoupon . ')';
                    $query = $this->db->query($strcoupon);
                }
            }
            $this->session->set_userdata('message', 'Data Coupon Updated <br/>');
            $this->session->set_userdata('messagestatus', '1');
        }
    }

}

?>