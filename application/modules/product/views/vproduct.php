<?php
$config = GetConfig();
$selectcurrency = SelectedCurrency();
?>
<div id="column-left">
    <?php left_category(@$cat_id, @$sub_cat_id); ?>
    <div id="banner0" class="banner">

        <div style="display: block;"><img src="http://www.thepaperstone.com/image/cache/data/180x180px free shipping-180x180.jpg" alt="HP Banner" title="HP Banner"></div>

    </div> 
</div>


<div id="content" class="inside_page">  
    <div class="breadcrumb">
        <a href="<?php echo base_url() ?>">Home</a>
        <?php if (!CheckEmpty(@$model)) { ?>
            »<a href="<?php echo base_url() . 'index.php/user/view_category/' . $model->product_category . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model->parentname) . '.html' ?>"><?php echo @$model->parentname ?></a>

            »<a href="<?php echo base_url() . 'index.php/user/view_sub_category/' . $model->product_sub_category_id . '?' . GetCurrencyPath(false, true) . 'name=' . preg_replace("/[^a-zA-Z0-9]+/", "-", $model->subname) . '.html' ?>"><?php echo @$model->subname ?></a>
            »<a href="javascript:;"><?php echo @$model->product_name ?></a>
        <?php } ?>
    </div>

    <div class="product-info">

        <div class="left">
            <div class="image">
                <?php if (CheckEmpty(@$model->listprice['stock'])) { ?>
                    <div class="outofstockbig" style="background: url('<?php echo base_url() ?>images/outofstockbig.png') left top no-repeat;"></div>
                <?php } ?>
                <div class="image_inside">
                    <div id="wrap" style="top:0px;z-index:1000;position:relative;">
                        <div class="zoom-section">    	  
                            <div class="zoom-small-image">
                                <input type="hidden" id="txt_id_product" value="<?php echo @$model->product_id ?>">
                                <a href="<?php echo $config['folderproduct'] . (@$model->product_image != '' ? @$model->product_image : 'default.jpg') ?>" title="<?php echo @$model->product_name ?>" class="cloud-zoom" id="zoom1" rel="adjustX:10, adjustY:-4" >
                                    <img style="width:310px;" src="<?php echo $config['folderproduct'] . (@$model->product_image != '' ? @$model->product_image : 'default.jpg') ?>" title="<?php echo @$model->product_name ?>" alt="<?php echo @$model->product_name ?>" id="image" ></a>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="zoom_btn">
                    <a id="zoomer" class="colorbox cboxElement" href="<?php echo $config['folderproduct'] . (@$model->product_image != '' ? @$model->product_image : 'default.jpg') ?>" style="display: inline;">Zoom</a>        
                </div></div>
            <div class="image-additional">


            <?php foreach (@$model->listimage as $imagestatuan) { ?>
                <a href='<?php echo $config['folderproduct'] . $imagestatuan['prodet_image'] ?>' class='cloud-zoom-gallery' title='Just Lines - Peekaboo Cat' rel="useZoom: 'zoom1', smallImage: '<?php echo $config['folderproduct'] . $imagestatuan['prodet_image'] ?>' "><img class="zoom-tiny-image" src="<?php echo $config['folderproduct'] . $imagestatuan['prodet_image'] ?>" alt = "Just Lines - Peekaboo Cat"/></a>
                <?php } ?>

            </div>
        </div>
        <script>
            $('a#zoomer').colorbox({rel: 'gal'});</script>
        <div class="right"> 
            <h1 class="pr_name"><?php echo @$model->product_name ?></h1>

            <div class="price">

                <span class="txt_price">Price:&nbsp;&nbsp;&nbsp;</span>

                <?php
                if (count(@$model->listprice) > 0) {
                    if (@$model->listprice['price'] > @$model->listprice['discountprice']) {
                        ?>
                        <span class="price-old"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, @$model->listprice['price']), $selectcurrency) ?></span>&nbsp;
                        <?php
                    }
                    ?>
                    <span class="price-new"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, @$model->listprice['discountprice']), $selectcurrency) ?></span>  
                <?php } else {
                    ?>

                    <span class="price-new"><?php echo DefaultCurrencyForView(ConvertCurrency($selectcurrency, @$model->product_unit_cost), $selectcurrency) ?></span>          

                <?php } ?>
                <div class="clear"></div>
                <span class="price-tax">GST Incl.</span><br>
            </div>


            <div class="description">
                <span>Product Code:</span>&nbsp;<?php echo @$model->code_prefix ?><br>
                <span>Availability:</span>&nbsp;
                <?php if (CheckEmpty(@$productsatuan->listprice['stock'])) { ?>
                    Out Stock
                <?php } else { ?>
                    In Stock
                <?php } ?>
            </div>
            <div class="cart">

                <div>Qty:          <input type="number" name="quantity" id="txt_qty" class="qty_input" size="2" value="<?php echo $min_stock; ?>" step="1" min="<?php echo $min_stock; ?>" max="99">
                    <input type="hidden" name="product_id" size="2" value="1037">
                    &nbsp;<input type="button" value="Add to Cart" id="button-cart" class="button">
                </div>
                <span class="cart_clearer"></span>
                <?php if (GetUserId() <> 0) { ?>
                    &nbsp;<a href="javascript:;" data-id="<?php echo @$model->product_id ?>" class="icon_plus wishlist_link">Add to Wish List</a> &nbsp;&nbsp;
                    <a href="javascript:;" data-id="<?php echo @$model->product_id ?>" class="icon_plus compare_link">Add to Compare</a>
                <?php } ?>
            </div>

        </div>
    </div>

    <div id="tabs" class="htabs"><a href="#tab-description" class="selected" style="display: inline;">Description</a>
        <a href="#tab-review" class="" style="display: inline;">Reviews (0)</a>
    </div>
    <div id="tab-description" class="tab-content" style="display: block;">
        <?php echo @$model->product_description ?>
    </div>
    <div id="tab-review" class="tab-content" style="display: none;">
        <div id="review"><div class="content">There are no reviews for this product.</div>
        </div>
        <h2 id="review-title">Write a review</h2>
        <div class="r_label">Your Name:</div>
        <input type="text" name="name" value="" class="ie_left">
        <div class="r_label">Your Review:</div>
        <textarea name="text" cols="40" rows="8" style="width: 98%;" class="ie_left"></textarea>
        <span style="font-size: 11px;"><span style="color: #FF0000;">Note:</span> HTML is not translated!</span><br>
        <br>
        <b class="r_label">Rating:</b> <span>Bad</span>&nbsp;
        <input type="radio" name="rating" value="1">
        &nbsp;
        <input type="radio" name="rating" value="2">
        &nbsp;
        <input type="radio" name="rating" value="3">
        &nbsp;
        <input type="radio" name="rating" value="4">
        &nbsp;
        <input type="radio" name="rating" value="5">
        &nbsp; <span>Good</span><br>
        <br>
        <div class="r_label">Enter the code in the box below:</div>
        <input type="text" name="captcha" value="" class="ie_left">
        <br>
        <img src="index.php?route=product/product/captcha" alt="" id="captcha"><br>
        <br>
        <div class="buttons">
            <div class="right"><a id="button-review" class="button">Continue</a></div>
        </div>
    </div>





</div>
<script>
    $(".wishlist_link").click(function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/tools/wishlistadd",
                    data: {
                        id_product: $(this).data("id")

                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            modaldialog(data.msg);
                        }
                        else
                        {
                            messageerror(data.msg);
                        }
                    },
                    error: function (xhr, status, error)
                    {

                        messageerror(xhr.responseText);
                    }
                });
        return false;
    })
    $("#button-cart").click(function () {
        $.ajax(
                {
                    url: baseurl + "/index.php/cart/addtocart",
                    data: {
                        product_id: $("#txt_id_product").val(),
                        qty: $("#txt_qty").val()
                    },
                    dataType: "json",
                    type: "post",
                    success: function (data)
                    {
                        if (data.st)
                        {
                            modaldialog(data.msg);
                            messagesuccess(data.msg);
                            RefreshCart();
                        }
                        else
                        {
                            messageerror(data.msg);
                        }

                    },
                    error: function (xhr, status, error)
                    {
                        messageerror(xhr.responseText);
                    }
                });
        return false;
    })
</script>    