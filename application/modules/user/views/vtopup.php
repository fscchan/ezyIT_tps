<?php
	$config = GetConfig();
	$topupoptions = json_decode(GetResponseFromAPI($config["membermenuurl"], array()), true);
	// $topupoptions = json_decode('[{"id": "1","type": "New Membership $55","commission_amount": "0.50","credit_amount": "55.00","price": "55.00","status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "2","type": "New Membership $107","commission_amount": "1.00","credit_amount": "115.00","price": "107.00","status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "3","type": "New Membership $500","commission_amount": "5.00","credit_amount": "550.00","price": "500.00","status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "4","type": "New Temporary Membership","commission_amount": "0.10","credit_amount": null,"price": null,"status": "1","create_date": null,"create_by": null,"edit_date": "2016-09-02 14:31:00","edit_by": null},{"id": "5","type": "Top Up - $25","commission_amount": "0.22","credit_amount": "25.00","price": "25.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-30 15:11:01","edit_by": "1"},{"id": "6","type": "Top Up - $55","commission_amount": "0.55","credit_amount": "58.00","price": "55.00","status": "1","create_date": null,"create_by": null,"edit_date": null,"edit_by": null},{"id": "7","type": "Top Up - $115","commission_amount": "1.00","credit_amount": "115.00","price": "107.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-22 10:11:57","edit_by": "1"},{"id": "8","type": "Top Up - $500","commission_amount": "5.00","credit_amount": "550.00","price": "500.00","status": "1","create_date": null,"create_by": null,"edit_date": "2016-11-22 10:11:57","edit_by": "1"}]', true);
?>
<div id="content">
	<div class="breadcrumb">
		<a href="<?php echo base_url() ?>">Home</a>
		» <a href="<?php echo base_url() . 'index.php/user/topup' ?>">Top-up credits</a>
	</div>
	<h1>Top-up credits</h1>
	<div class="cart-info">
		<table style="width: 80%; margin: 0px auto 16px;">
			<thead>
				<tr>
					<td colspan="4"><h2 style="text-align: center;">Choose top-up value</h2></td>
				</tr>
				<tr>
					<td></td>
					<td>Name</td>
					<td>Credits</td>
					<td>Price</td>
				</tr>
			</thead>
			<tbody>
				<?php
					foreach ($topupoptions as $topupoption) {
						if (preg_match("/^(Top Up)/i", $topupoption["type"]) && $topupoption["status"] == 1) {
							echo "<tr>
								<td><input type=\"radio\" name=\"topup_option\" id=\"topup_" . number_format($topupoption["credit_amount"], 0) . "\" value='" . $regtype["id"] . "' required></td>
								<td><label for=\"topup_" . number_format($topupoption["credit_amount"], 0) . "\" style=\"display: block;\">" . $topupoption["type"] . "</label></td>
								<td><label for=\"topup_" . number_format($topupoption["credit_amount"], 0) . "\" style=\"display: block;\">" . $topupoption["credit_amount"] . "</label></td>
								<td><label for=\"topup_" . number_format($topupoption["credit_amount"], 0) . "\" style=\"display: block;\">" . DefaultCurrencyForView(ConvertCurrency(SelectedCurrency(), $topupoption["price"]), SelectedCurrency()) . "</label></td>
							</tr>";
						}
					}
				?>
			</tbody>
		</table>
		<div class="right" style="width: 80%; margin: 0px auto 24px;"><input type="button" name="topup_submit" id="topup_submit" class="button" value="Top-up using PayPal"></div>	
		<form id="order_detail" style="display: none;" method="post">
			<input type="hidden" name="no_note" value="1">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="no_shipping" value="1">
			<input type="hidden" name="item_number" value="TP">
			<input type="hidden" name="currency_code" value="SGD">
			<input type="hidden" name="typepayment" value="paypal">
			<input type="hidden" name="cancel_return" value="<?php echo base_url(); ?>index.php/tools/account">
			<input type="hidden" name="return">
			<input type="hidden" name="amount">
			<input type="hidden" name="custom">
			<input type="hidden" name="business">
			<input type="hidden" name="item_name">
			<input type="hidden" name="image_url"> 
			<input type="hidden" name="cpp_logo_image">
			<input type="hidden" name="cpp_header_image">
			<table style="width: 80%; margin: 0px auto 16px;">
				<thead>
					<tr><td colspan="2"><h2 style="text-align: center;">Transaction detail</h2></td></tr>
				</thead>
				<tbody>
					<tr><th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Order type:</th><td><input type="text" id="order_type" style="width: 100%; background: none; border: none; box-shadow: none; font-family: inherit; color: inherit; font-size: inherit;" value="" readonly></td></tr>
					<tr><th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Invoice:</th><td><input type="text" id="order_invoice" style="width: 100%; background: none; border: none; box-shadow: none; font-family: inherit; color: inherit; font-size: inherit;" value="" readonly></td></tr>
					<tr><th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Date:</th><td><input type="text" id="order_date" style="width: 100%; background: none; border: none; box-shadow: none; font-family: inherit; color: inherit; font-size: inherit;" value="" readonly></td></tr>
					<tr><th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Cost:</th><td><input type="text" id="order_cost" style="width: 100%; background: none; border: none; box-shadow: none; font-family: inherit; color: inherit; font-size: inherit;" value="" readonly></td></tr>
					<tr><td style="text-align: right;" colspan="2"><input type="submit" name="topup_continue" id="topup_continue" class="button" value="Continue" onclick="window.location.href = baseurl + 'index.php/tools/account';"></td></tr>
				</tbody>
			</table>
		</form>
	</div>
</div>
<script>
	$("#topup_submit").click(function(){
		if ($("[name=preg_option]:checked").length != 0) {
			$.ajax({
				url: baseurl + "index.php/user/preg",
				dataType: "json",
				type: "post",
				data: {
					param: $("[name=topup_option]:checked").val()
				},
				success: function(data) {
					console.log(data); // delete
					if (data.st) {
						$("#preg_submit").prop("disabled", "disabled");
						$("[name=preg_option]").prop("disabled", "disabled");
						$("[name=custom]").val(data.datareturns.custom);
						$("[name=return]").val(data.datareturns.return);
						$("[name=amount]").val(data.details.totalamount);
						$("[name=business]").val(data.datareturns.account_paypal);
						$("[name=image_url]").val(data.datareturns.cpp_header_image);
						$("[name=item_name]").val("ThePaperStone Membership - " + data.details.order_number);
						$("[name=item_number]").val(data.details.order_number);
						$("[name=cpp_logo_image]").val(data.datareturns.cpp_header_image);
						$("[name=cpp_header_image]").val(data.datareturns.cpp_header_image);
						if (/^(TP-)/.test(data.details.order_number)) {
							$("#order_type").val("Top up membership");
						} else if (/^(PM-)/.test(data.details.order_number)) {
							$("#order_type").val("Premium membership registration");
						} else {
							$("#order_type").val("Invalid");
						}
						$("#order_date").val(data.details.humandate);
						$("#order_invoice").val(data.details.order_number);
						$("#order_cost").val(data.details.totalamount);
						$("#order_cancellation").val(baseurl + "index.php/tools/canceltrans/" + data.details.order_number);
						$("#order_detail").css("display", "block").prop("action", data.paypal.url);
						messagesuccess(data.msg + "<br/>");
					} else {
						messageerror("Error.");
					}
				},
				error: function (xhr, status, error) {
					messageerror(xhr.responseText);
				}
			});
		} else {
			messageerror("Please select something.<br/>");
		}
	});
</script>