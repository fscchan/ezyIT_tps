


<div id="column-left">
    <div class="box">
        <div class="box-heading">Account</div>
        <div class="box-content">
            <ul>
                <?php if (GetUserId() == '0') { ?>
                    <li><a href="<?php echo base_url() . 'index.php/user/login' ?>">Login</a>
                    <li><a href="<?php echo base_url() . 'index.php/user/forgot' ?>">Forgotten Password</a></li>
                     <li><a href="<?php echo base_url() . 'index.php/tools/activate' ?>">Activate  Account</a></li>
                <?php } else { ?>
                    <li <?php echo $acc=="info"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/account' ?>">My Account</a></li>
                    <li <?php echo $acc=="edit"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/edit' ?>">Edit Account</a></li>
                    <li <?php echo $acc=="change"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/changepassword' ?>">Change Password</a></li>
                    <li <?php echo $acc=="address"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/addressbook' ?>">Address Books</a></li>
                    <li <?php echo $acc=="whis"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/wishlist' ?>">Wish List</a></li>
                    <li <?php echo $acc=="order"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/orderhistory' ?>">Order History</a></li>
                    <li><a href="<?php echo base_url() . 'index.php/tools/transaction' ?>">Transactions</a></li>
                    <li <?php echo $acc=="news"?'class="active"':''?>><a href="<?php echo base_url() . 'index.php/tools/newsletter' ?>">Newsletter</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>