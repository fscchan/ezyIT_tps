<?php
	$config = GetConfig();
	$membermenus = json_decode(GetResponseFromAPI($config["membermenuurl"], array()), true);
	// $membermenus = json_decode('[{"id":"1","type":"New Membership $55","commission_amount":"0.50","credit_amount":null,"price":null,"status":"1","create_date":null,"create_by":null,"edit_date":null,"edit_by":null},{"id":"2","type":"New Membership $107","commission_amount":"1.00","credit_amount":null,"price":null,"status":"1","create_date":null,"create_by":null,"edit_date":null,"edit_by":null},{"id":"3","type":"New Membership $500","commission_amount":"5.00","credit_amount":null,"price":null,"status":"1","create_date":null,"create_by":null,"edit_date":null,"edit_by":null},{"id":"4","type":"New Temporary Membership","commission_amount":"0.10","credit_amount":null,"price":null,"status":"1","create_date":null,"create_by":null,"edit_date":"2016-09-02 14:31:00","edit_by":null},{"id":"5","type":"Top Up - $25","commission_amount":"0.22","credit_amount":"25.00","price":"25.00","status":"1","create_date":null,"create_by":null,"edit_date":"2016-11-30 15:11:01","edit_by":"1"},{"id":"6","type":"Top Up - $55","commission_amount":"0.55","credit_amount":"58.00","price":"55.00","status":"1","create_date":null,"create_by":null,"edit_date":null,"edit_by":null},{"id":"7","type":"Top Up - $115","commission_amount":"1.00","credit_amount":"115.00","price":"107.00","status":"1","create_date":null,"create_by":null,"edit_date":"2016-11-22 10:11:57","edit_by":"1"},{"id":"8","type":"Top Up - $500","commission_amount":"5.00","credit_amount":"550.00","price":"500.00","status":"1","create_date":null,"create_by":null,"edit_date":"2016-11-22 10:11:57","edit_by":"1"}]', true);
	$data = json_decode(json_encode($data), true);
	$membermenu = $membermenus[$data["item_number"] - 1];
?>
<div id="content">  
	<div class="breadcrumb">
		<a href="<?php echo base_url(); ?>">Home</a>
		» <a href="<?php echo base_url() . "index.php/tools/account"; ?>">Account</a>
		» <a href="<?php echo base_url() . "index.php/user/checktrans/" , $invoice; ?>">Checking transaction: <?php echo $invoice; ?></a>
	</div>
	<h1>Checking transaction: <?php echo $invoice; ?></h1>
	<div class="cart-info">
		<table style="width: 80%; margin: 0px auto 16px;">
			<thead>
				<tr>
					<td colspan="4"><h2 style="text-align: center;">Transaction number: <?php echo $invoice; ?></h2></td>
				</tr>
				<tr>
					<td></td>
					<td>Information</td>
				</tr>
			</thead>
			<tbody>
				<?php if($data != null) { ?>
					<tr>
						<th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Date of order:</th>
						<td><?php echo $data["dateorder"]; ?></td>
					</tr>
					<tr>
						<th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Item ordered:</th>
						<td><?php echo $membermenu["type"]; ?></td>
					</tr>
					<tr>
						<th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Price:</th>
						<td><?php echo @DefaultCurrencyForView($membermenu["price"], SelectedCurrency()); ?></td>
					</tr>
					<tr>
						<th style="text-align: right; background-color: #F7F7F7; padding: 0px 16px; border-right: 1px solid #eeeeee; border-bottom: 1px solid #eeeeee;">Status:</th>
						<td><?php echo ($data["status_order"] == 0 ? "Active" : ($data["status_order"] == 1 ? "Paid" : "Invalid")); ?></td>
					</tr>
				<?php } else { ?>
					<tr>
						<td colspan="2" style="font-size: 1.4em; text-align: center;"><b>Error</b><br/>Transaction not found</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>