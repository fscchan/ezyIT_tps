<?php

class M_paypal extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function call($data) {
        $config = GetConfig();
        if ($config['isproduction'] == '1') {
            $api_url = 'https://api-3t.sandbox.paypal.com/nvp';
        } else {
            $api_url = 'https://api-3t.paypal.com/nvp';
        }
        $api_user = $config['APIUsername'];
        $api_password = $config['APIPassword'];
        $api_signature = $config['APISignature'];
        $settings = array(
            'USER' => $api_user,
            'PWD' => $api_password,
            'SIGNATURE' => $api_signature,
            'VERSION' => '124.0',
            'METHOD'=>'SetExpressCheckout',
            'PAYMENTREQUEST_0_PAYMENTACTION'=>'Sale' 
        );

        

        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $api_url,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1",
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_POSTFIELDS => http_build_query(array_merge($data, $settings), '', "&"),
        );

        $ch = curl_init();

        curl_setopt_array($ch, $defaults);
        $result = curl_exec($ch);
       
        curl_close($ch);

        return $this->cleanReturn($result);
    }

    public function cleanReturn($data) {
        $data = explode('&', $data);

        $arr = array();

        foreach ($data as $k => $v) {
            $tmp = explode('=', $v);
            $arr[$tmp[0]] = isset($tmp[1]) ? urldecode($tmp[1]) : '';
        }

        return $arr;
    }

}
