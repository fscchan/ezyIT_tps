<?php

class Tools extends CI_Controller {

    public function cancelpayment($id) {

        // paypal api
		// if (paypal api return success) {
			// things to do
		// }
    }

    
    public function checkorder($id){
        $this->load->model("m_order");
        $model = $this->m_order->GetOneOrder($id);
      
        if(GetUserId()<>$model->id_customer_ecommerce&&  GetUserId()!=0)
        {
           redirect(base_url());
        }
        else
        {
             $this->m_order->Activation($id);
        }
        LoadTemplate($model, 'tools/vaccount_check_order', array());
        
    }
    public function wishlistadd()
    {
        $model = $this->input->post();
        $this->load->model("m_product");
        $message = '';
        $data = array();
        $data['st'] = FALSE;
        $this->form_validation->set_rules('id_product', "Product", 'required');
        

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if (GetUserId() != 0) {
                
                $data = $this->m_product->WishListAdd($this->input->post("id_product"));
            } else {
                $data['id'] = 0;
                $data['msg'] = "Your session has ecpired, try for relogin or reload the page";
            }
        }
        echo json_encode($data);
    }
    public function emailview() {
        $this->load->model("m_order");
        $model = $this->m_order->GetOneOrder(1);
        $this->load->view("email/v_email_order_done", $model);
    }

    public function account() {
        $model = array();
        $model['title'] = 'My Account';
        CheckSession();
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount', $javascript);
    }

    function addressbookmanipulate() {

        $model = $this->input->post();
        $this->load->model("user/m_user");
        $message = '';
        $data = array();
        $data['st'] = FALSE;
        $this->form_validation->set_rules('first_name', "First Name", 'required');
        $this->form_validation->set_rules('last_name', "Last Name", 'required');
        $this->form_validation->set_rules('telpnum', "Telephone", 'required');
        $this->form_validation->set_rules('address', "Address", 'required');
        $this->form_validation->set_rules('city', "City", 'required');
        $this->form_validation->set_rules('post_code', "Post Code", 'required');
        $this->form_validation->set_rules('country_id', "Country", 'required');
        $this->form_validation->set_rules('state_id', "Region", 'required');

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if (GetUserId() != 0) {
                $this->m_user->manipulateaddress_book($model);
                $data['st'] = true;
            } else {
                $data['id'] = 0;
                $data['msg'] = "Some Error Occured";
            }
        }
        echo json_encode($data);
    }

    function editaddress($id) {
        CheckSession();
        $model = array();
        $model['title'] = 'Edit Address';
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        $this->load->model("user/m_area");
        $this->load->model("user/m_user");
        $model['model'] = $this->m_user->GetOneAddressBook($id);

        if ($model['model'] == null) {
            redirect(base_url() . 'index.php/tools/newaddress');
        } else {
            if ($model['model']->id_member <> GetUserId()) {
                redirect(base_url() . 'index.php/tools/newaddress');
            }
        }
        $model['listcountry'] = $this->m_area->GetAllCountries();
        LoadTemplate($model, 'tools/vaccount_address_manipulate', $javascript);
    }
    function newaddress() {
        CheckSession();
        $model = array();
        $model['title'] = 'Edit Address';
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        $this->load->model("user/m_area");
        $this->load->model("user/m_user");
        
        $model['listcountry'] = $this->m_area->GetAllCountries();
        LoadTemplate($model, 'tools/vaccount_address_manipulate', $javascript);
    }
    public function deleteaddress() {
        $message = '';
        $this->load->model("user/m_user");
        $data = array();
        $this->form_validation->set_rules('id_addres', "Address", 'required');
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $this->m_user->DeleteAddress($this->input->post("id_addres"));
            SetMessageSession(1, "Delete has been deleted successfully");
            $data['st'] = $message == '';
        }
        echo json_encode($data);
    }
    public function deletewhishlist(){
        $data = array();
        $data['st'] = FALSE;
        $this->load->model("m_product");
        $this->form_validation->set_rules('product_id', "Product", 'required');
        $message='';

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if (GetUserId() != 0) {
                
                $this->m_product->WishListDelete($this->input->post("product_id"));
                 $data['st'] =true;
            } else {
                $data['id'] = 0;
                $data['msg'] = "Your session has ecpired, try for relogin or reload the page";
            }
        }
        echo json_encode($data);
    }
    public function addressbook() {
        CheckSession();
        $model = array();
        $model['title'] = 'Addres Book';
        $javascript = array();
        $this->load->model("user/m_user");
        $model['listaddress'] = $this->m_user->GetAddressBook(GetUserId());
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_addressbook', $javascript);
    }

    public function wishlist() {
        CheckSession();
        $this->load->model("m_product");
        $model = array();
        $model['title'] = 'Addres Book';
        $javascript = array();
        $this->load->model("user/m_user");
        $model['listwhislist'] =$this->m_product->GetWishlist();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_wish_list', $javascript);
    }

    public function orderhistory() {
        CheckSession();
         $this->load->model("m_product");
        $model = array();
        $model['title'] = 'Addres Book';
        $javascript = array();
        $this->load->model("user/m_user");
        $model['listwhislist'] =$this->m_product->GetWishlist();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_order_history', $javascript);
    }

    public function transaction() {
        CheckSession();
    }
    public function subscribe_action(){
        $email=$this->input->post("email");
         $this->load->model("user/m_user");
         if($this->input->post("newsletter")=="0")
        {
            $this->m_user->UnSubscribe($email);
        }
        else {
            $objmodel=  GetMemberData();
            $arrayinput=array();
            $arrayinput['salutation']='';
            $arrayinput['first_name']=$objmodel->first_name;
            $arrayinput['last_name']=$objmodel->last_name;
            $arrayinput['name']=$objmodel->first_name.' '.$objmodel->last_name;
            $arrayinput['email']=$objmodel->email;
            $this->m_user->Subscribe($arrayinput);
        }
        SetMessageSession(1, 'Subscriber Updated');
        echo json_encode(array("st"=>true));
        
        
        
    }
    
    
    
    
    public function newsletter() {
        CheckSession();
         $this->load->model("m_product");
        $model = array();
        $model['title'] = 'Newsletter';
        $javascript = array();
        $this->load->model("user/m_user");
        $objmodel = GetMemberData();
        $model['mail']=$objmodel->email;
        $model['issuscribe']=$this->m_user->IsSubscribe($model['mail']);
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_subscribe', $javascript); 
    }

    public function activate_sent() {
        $message = '';
        $email = $this->input->post("email");
        $this->load->model("m_email");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->CheckActivation($email);
            if ($message == '') {
                $this->m_email->SendEmailActivitation($email);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function new_password_action() {
        $model = $this->input->post();

        $message = '';
        $data = array();
        $data['st'] = false;
        $this->lang->load("user");
        $this->form_validation->set_rules('password', "Password", 'required');
        $this->form_validation->set_rules('confirm', "Confirm Password", 'required|matches[password]');

        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            if ($model['member_id'] != GetUserId()) {
                $message.='This action cannot be completed because you need to refresh your account<br/>';
            } else {
                $this->load->model('user/m_user');
                $this->m_user->set_new_password($model['password']);
                $this->session->set_userdata("ischangepassword", false);
            }
            $data['st'] = $message == '';
            $data['msg'] = $message;
        }
        echo json_encode($data);
        exit;
    }

    public function newpassword() {

        $session = $this->session->userdata('ischangepassword');
        if ($session != null && $session = false) {
            redirect(base_url() . 'index.php/tools/account');
        }
        LoadTemplate(array(), 'tools/vaccount_new_password');
    }

    public function activationdirectly() {
        $email = $this->input->get("email");
        $activation = $this->input->get("activation");
        $this->load->model("user/m_user");


        $message = $this->m_user->CheckActivation($email);
        if ($message == '') {
            $encrypt = sha1(strtolower($email));
            if ($encrypt != $activation) {
                $message.='Your Activation Code is wrong<br/>';
            } else {
                $user = $this->m_user->GetOneUserFromEmail($email);
                $this->m_user->activate($email);
                $newuser = $this->m_user->GetOneUserFromEmail($email);

                if ($user == null || CheckEmpty($user->password)) {
                    SetUserId($newuser->id_customer_ecommerce);
                    SetUsernameUser($newuser->email);
                    $this->session->set_userdata('ischangepassword', true);
                    redirect(base_url() . 'index.php/tools/newpassword');
                } else {

                    SetMessageSession(1, 'Your Account has been acivated');
                    SetUserId($user->id_customer_ecommerce);
                    SetUsernameUser($user->email);
                    redirect(base_url() . 'index.php/user/message');
                }
            }
        }


        echo $message;
    }

    public function recoverdirectly() {
        $email = $this->input->get("email");
        $activation = $this->input->get("activation");
        $this->load->model("user/m_user");
        $message = '';

        $row = $this->m_user->GetOneUserFromEmail($email);
        if ($row != null) {
            if ($row->forgot_password != $activation || $row->forgot_password == '') {
                $message.='Your Recover Code is wrong<br/>';
            } else {
                SetUserId($row->id_customer_ecommerce);
                SetUsernameUser($row->email);
                $this->session->set_userdata('ischangepassword', true);
                redirect(base_url() . 'index.php/tools/newpassword');
            }
        }


        echo $message;
    }

    public function activate_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Activation Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $message = $this->m_user->CheckActivation($email);
            if ($message == '') {
                $encrypt = sha1(strtolower($email));
                if ($encrypt != $activationcode) {
                    $message.='Your Activation Code is wrong<br/>';
                } else {
                    $url.=base_url() . 'index.php/tools/activationdirectly?email=' . strtolower($email) . '&&activation=' . $encrypt;
                }
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function changepasswordaction() {
        $this->form_validation->set_rules('email', "Email", 'required');
    }

    public function forgot_process() {
        $message = '';
        $email = $this->input->post("email");
        $activationcode = $this->input->post("activationcode");
        $data['st'] = FALSE;
        $this->form_validation->set_rules('email', "Email", 'required');
        $this->form_validation->set_rules('activationcode', "Recover Code", 'required');
        $url = '';
        $this->load->model("user/m_user");
        if ($this->form_validation->run() === FALSE || $message != '') {
            $data['msg'] = 'Error :' . validation_errors() . $message;
        } else {
            $row = $this->m_user->GetOneUserFromEmail($email);

            if ($row != null) {

                if ($row->forgot_password == '') {
                    $message.='Your Recover Code is expired , you can retry for recover your password again by click form above<br/>';
                }
                if ($message == '') {
                    if ($row->forgot_password != $activationcode) {
                        $message.='Your Recover Code is wrong<br/>';
                    } else {
                        $url.=base_url() . 'index.php/tools/recoverdirectly?email=' . strtolower($email) . '&&activation=' . $activationcode;
                    }
                }
            } else {
                $message.='We Cannot found your profil from our database<br/>';
            }
            $data['st'] = $message == '';
            $data['url'] = $url;
            $data['msg'] = $message != '' ? $message : "We have sent email step for activation your account";
        }
        echo json_encode($data);
    }

    public function edit() {
        $model = array();
        CheckSession();
        $model['title'] = 'Edit Profile';
        $javascript = array();
        $this->load->model("user/m_area");
        $model['listcountry'] = $this->m_area->GetAllCountries();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_edit', $javascript);
    }

    public function changepassword() {
        $model = array();
        CheckSession();
        $model['title'] = 'Edit Profile';
        $javascript = array();
        $this->load->model("user/m_area");
        $model['listcountry'] = $this->m_area->GetAllCountries();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_change_password', $javascript);
    }

    public function activate() {
        $model = array();
        $model['title'] = 'Edit Profile';
        $javascript = array();
        $javascript[0] = "asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'tools/vaccount_activate', $javascript);
        
    }
    
	public function checktrans($iv) {
		if (!empty($this->input->get("token")) && !empty($this->input->get("PayerID")) && !empty($this->session->userdata("paypaltoken")) && !empty($this->session->userdata("transiv")) && $iv == $this->session->userdata("transiv")) {
			$this->db->where("order_number", $this->session->userdata("transiv"));
			$this->db->update("tbl_ecommerce_pmembership", array("status_order" => 1)); 
		}
		$this->db->from('tbl_ecommerce_pmembership');
		$this->db->where(array("order_number" => $iv, "id_customer_ecommerce" => GetUserId()));
		$model["invoice"] = $iv;
		$model["data"] = $this->db->get()->row();
        LoadTemplate($model, 'tools/vaccount_checktrans', array());
		if (!empty($this->input->get("token")) && !empty($this->input->get("PayerID")) && !empty($this->session->userdata("paypaltoken")) && !empty($this->session->userdata("transiv")) && $iv == $this->session->userdata("transiv")) {
			$config = GetConfig();
			$membermenus = json_decode(GetResponseFromAPI($config["membermenuurl"], array()), true);
			$mstats = GetMembership();
			$user = json_decode(json_encode(GetMemberData()), true);
			$now = date("Y-m-d H:i:s");
			$this->db->from('tbl_ecommerce_pmembership');
			$this->db->where(array("order_number" => $this->session->userdata("transiv")));
			$order = $this->db->get()->row();
			$membermenu = $membermenus[$order["item_number"] - 1];
			if ($mstats === null) {
				$insert["membership_card_no"] = "";
				$insert["type"] = "1";
				$insert["valid_date"] = null;
				$insert["name"] = $user["first_name"] . " " . $user["last_name"];
				$insert["password"] = $user["password"];
				$insert["IC"] = "";
				$insert["credits"] = $membermenu["credit_amount"];
				$insert["email"] = $user["email"];
				$insert["birth_month"] = "";
				$insert["mobile_phone"] = $user["telephone"];
				$insert["use_password"] = "off";
				$insert["location_id"] = "";
				$insert["status"] = 1;
				$insert["create_date"] = $now;
				$insert["create_by"] = 0;
				$insert["edit_date"] = $now;
				$insert["edit_by"] = 1;
				$insert["closure_date"] = "0000-00-00 00:00:00";
				$insert["receipt_number"] = $order["order_number"];
				$response = json_decode(GetResponseFromAPI($config["loginurl"], array(), "POST", $insert), true);
				$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			} else {
				$oldmember = json_decode(GetResponseFromAPI($config["loginurl"], array("email" => $user["email"])), true);
				$update["credits"] = $oldmember["credits"] + $membermenu["credit_amount"];
				$update["edit_date"] = $now;
				$update["receipt_number"] = $order["order_number"];
				$response = json_decode(GetResponseFromAPI($config["loginurl"], array("data"=>array("data"=>array("email"=>$user["email"]))), "PUT", $update), true);
				$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			}
			$this->session->unset_userdata("paypaltoken");
			$this->session->unset_userdata("transiv");
		}
	}
    
	public function canceltrans($iv = null) {
		echo "Cancel? invoice: " . $iv;
	}
	
	/* public function activatetrans() {
		if (!empty($this->input->get("token")) && !empty($this->input->get("PayerID")) && !empty($this->session->userdata("paypaltoken")) && !empty($this->session->userdata("transiv"))) {
			$config = GetConfig();
			$membermenus = json_decode(GetResponseFromAPI($config["membermenuurl"], array()), true);
			$mstats = GetMembership();
			$user = json_decode(json_encode(GetMemberData()), true);
			$now = date("Y-m-d H:i:s");
			$this->db->from('tbl_ecommerce_pmembership');
			$this->db->where(array("order_number" => $this->session->userdata("transiv")));
			$order = $this->db->get()->row();
			$membermenu = $membermenus[$order["item_number"] - 1];
			if ($mstats === null) {
				$insert["membership_card_no"] = "";
				$insert["type"] = "1";
				$insert["valid_date"] = null;
				$insert["name"] = $user["first_name"] . " " . $user["last_name"];
				$insert["password"] = $user["password"];
				$insert["IC"] = "";
				$insert["credits"] = $membermenu["credit_amount"];
				$insert["email"] = $user["email"];
				$insert["birth_month"] = "";
				$insert["mobile_phone"] = $user["telephone"];
				$insert["use_password"] = "off";
				$insert["location_id"] = "";
				$insert["status"] = 1;
				$insert["create_date"] = $now;
				$insert["create_by"] = 0;
				$insert["edit_date"] = $now;
				$insert["edit_by"] = 1;
				$insert["closure_date"] = "0000-00-00 00:00:00";
				$insert["receipt_number"] = $order["order_number"];
				$response = json_decode(GetResponseFromAPI($config["loginurl"], array(), "POST", $insert), true);
				$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			} else {
				$oldmember = json_decode(GetResponseFromAPI($config["loginurl"], array("email" => $user->email)), true);
				$update["credits"] = $oldmember["credits"] + $membermenu["credit_amount"];
				$update["edit_date"] = $now;
				$update["receipt_number"] = $order["order_number"];
				$response = json_decode(GetResponseFromAPI($config["loginurl"], array("data"=>array("data"=>array("email"=>$user["email"]))), "PUT", $update), true);
				$transaction = json_decode(GetResponseFromAPI($config["membertransaction"], array("member_id"=>$mstats["member_id"])), true);
			}
			$this->session->unset_userdata("paypaltoken");
			$this->session->unset_userdata("transiv");
			die(json_encode(array("st" => true, "msg" => "Your membership has been activated")));
		} else {
			die(json_encode(array("st" => false, "msg" => "Nothing to do")));
		}
	} */
}
