<?php

class Home extends CI_Controller {

    public function index() {
        $model = array();
        $model['title'] = 'Home';
        $this->load->model("m_slider");
        $this->load->model("m_banner");
        $this->load->model("m_product");
        $model['slider']=$this->m_slider->GetAllSlider();

        $model['main_banner']=$this->m_banner->GetMainBanner();
        
        $model['banner']=$this->m_banner->GetSubMainBanner();

        $javascript=array();
        $javascript[0]="asset/user/js/jquery.nivo.slider.pack.js";
        LoadTemplate($model, 'home/vhome',$javascript);
    }
    function search(){

        $search = $this->input->get("search");

        $model = array();
        $model['title'] = 'Home';
        $this->load->model("m_product");

        $config = array();
        $config["base_url"] = base_url() . "index.php/modules/search/search?search=".$search;

        $model["search"] = $this->m_product->search_keyword($search); 

        $model['keyword'] = $search;

        LoadTemplate($model, 'home/vsearching');

    }

}
