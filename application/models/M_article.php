<?php

class M_article extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    public function GetOneArticle($id){
    	$this->db->select('*');
        $this->db->join('tbl_ecommerce_cattegory_article b', 'a.id_category = b.id_category');
        $this->db->where('a.id_article', $id);
        $query = $this->db->get('tbl_ecommerce_articles a');

        return $query->result_array();
    }
 }


?>