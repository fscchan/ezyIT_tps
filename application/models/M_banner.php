<?php

class M_banner extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function GetAllBanner() {
        $this->db->from("tbl_banner");
        $listbanner = $this->db->get()->result();
        
        return $listbanner;
    }

    function GetMainBanner(){
        $this->db->from("tbl_banner");
        $this->db->where("tbl_banner.jenis",'1');
        $this->db->where("tbl_banner.isactive",'1');

        $listbanner = $this->db->get()->result();
        return $listbanner;
    }

    function GetSubMainBanner(){
    	$this->db->from("tbl_banner");
    	$this->db->where("tbl_banner.jenis",'2');
        $this->db->where("tbl_banner.isactive",'1');

    	$listbanner = $this->db->get()->result();
    	return $listbanner;
    }

    function GetCategoryBanner($cat_id = 0){
        $this->db->from("tbl_banner");
        $this->db->where("cat_id",$cat_id);
        $this->db->where("id_sub_category",'0');

        $listbanner = $this->db->get()->result();
        return $listbanner;
    }

    function GetSubCategoryBanner($sub_cat_id = 0){
        $this->db->from("tbl_banner");
        $this->db->where("id_sub_category",$sub_cat_id);

        $listbanner = $this->db->get()->result();
        return $listbanner;
    }

}

?>